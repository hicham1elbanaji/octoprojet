package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class BanqueServiceTest {

	@MockBean
	CompteRepository compteRepository;
	@MockBean
	VirementRepository virementRepository;
	@Autowired
	BanqueServiceImpl banqueService;

	@Test
	public void testLoadAllVirement() throws Exception {

		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setFirstname("firstName");
		utilisateur.setGender("homme");
		utilisateur.setLastname("lastName");

		Compte compteBeneficiaire = new Compte();
		compteBeneficiaire.setNrCompte("55543");
		compteBeneficiaire.setRib("33445");
		compteBeneficiaire.setSolde(new BigDecimal(44000));
		compteBeneficiaire.setUtilisateur(utilisateur);

		Compte compteEmetteur = new Compte();
		compteBeneficiaire.setNrCompte("355543");
		compteBeneficiaire.setRib("9990875");
		compteBeneficiaire.setSolde(new BigDecimal(55000));
		compteBeneficiaire.setUtilisateur(utilisateur);

		Virement virement = new Virement();
		virement.setDateExecution(new Date());
		virement.setMontantVirement(new BigDecimal(3000));
		virement.setMotifVirement("virement");
		virement.setCompteBeneficiaire(compteBeneficiaire);
		virement.setCompteEmetteur(compteEmetteur);

		List<Virement> allVirements = Arrays.asList(virement, virement, virement);

		when(virementRepository.findAll()).thenReturn(allVirements);

		assertEquals(3, banqueService.loadAllVirement().size());
		verify(virementRepository, times(1)).findAll();

	}

	@Test
	public void testCreateVersement() throws Exception {

		VersementDto versementDto = new VersementDto();
		versementDto.setMontantVirement(new BigDecimal(300));
		versementDto.setRibCompteBeneficiaire("3332");
		versementDto.setMotifVersement("Versement");

		when(compteRepository.findByRib("3332")).thenReturn(null);

		Exception exception = assertThrows(CompteNonExistantException.class, () -> {
			banqueService.createVersement(versementDto);
		});

		String expectedMessage = "Compte Beneficiaire Non existant";

		assertTrue(expectedMessage.equals(exception.getMessage()));
	}

}
