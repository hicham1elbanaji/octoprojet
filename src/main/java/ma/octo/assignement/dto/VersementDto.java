package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VersementDto {

	  private String nom_prenom_emetteur;
	  private String RibCompteBeneficiaire;
	  private String motifVersement;
	  private BigDecimal montantVirement;
	  private Date date;
	  
}
