package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    private static VirementDto virementDto;

    public static VirementDto virementToVirementDto(Virement virement) {
        virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());

        return virementDto;

    }
    
   public static VersementDto versementToVersementDto(Versement versement) {
	   VersementDto versementDto = new VersementDto();
	   versementDto.setDate(versement.getDateExecution());
	   versementDto.setRibCompteBeneficiaire(versement.getCompteBeneficiaire().getRib());
	   versementDto.setMontantVirement(versement.getMontantVirement());
	   versementDto.setMotifVersement(versement.getMotifVersement());
	   versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
	   
	   return versementDto;
   }
}
