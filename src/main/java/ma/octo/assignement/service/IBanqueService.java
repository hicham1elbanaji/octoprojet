package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface IBanqueService {

	List<VirementDto> loadAllVirement();
	
	List<VersementDto> loadAllVersement();

	List<Compte> loadAllCompte();

	List<Utilisateur> loadAllUtilisateur();

	void createVirement(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;
	
	void createVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException;
}
