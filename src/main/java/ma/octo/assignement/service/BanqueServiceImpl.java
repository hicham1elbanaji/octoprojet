package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;

@Service
@Transactional
public class BanqueServiceImpl implements IBanqueService {

	public static final int MONTANT_MAXIMAL = 10000;
	private int virmentMax=10;

	CompteRepository compteRepository;
	VirementRepository virementRepository;
	VersementRepository versementRepository;
	UtilisateurRepository utilisateurRepository;

	AuditService auditService;

	public BanqueServiceImpl(CompteRepository compteRepository, VirementRepository virementRepository,
			VersementRepository versementRepository, UtilisateurRepository utilisateurRepository,
			AuditService auditService) {

		this.compteRepository = compteRepository;
		this.virementRepository = virementRepository;
		this.utilisateurRepository = utilisateurRepository;
		this.auditService = auditService;
		this.versementRepository = versementRepository;
	}

	Logger LOGGER = LoggerFactory.getLogger(BanqueServiceImpl.class);

	@Override
	public List<VirementDto> loadAllVirement() {
		List<Virement> virements = virementRepository.findAll();
		List<VirementDto> virementDtos = virements.stream().map(ele -> VirementMapper.virementToVirementDto(ele))
				.collect(Collectors.toList());
		return virementDtos.isEmpty() ? null : virementDtos;
	}
	
	@Override
	public List<VersementDto> loadAllVersement() {
		List<Versement> versements = versementRepository.findAll();
		List<VersementDto> versementDtos = versements.stream().map(ele -> VirementMapper.versementToVersementDto(ele))
				.collect(Collectors.toList());
		return versementDtos.isEmpty() ? null : versementDtos;
	}

	@Override
	public List<Compte> loadAllCompte() {
		List<Compte> comptes = compteRepository.findAll();
		return comptes.isEmpty() ? null : comptes;
	}

	@Override
	public List<Utilisateur> loadAllUtilisateur() {
		List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
		return utilisateurs.isEmpty() ? null : utilisateurs;
	}

	@Override
	public void createVirement(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		
		
		Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
		Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());
		BigDecimal montantVirement = virementDto.getMontantVirement();

		String motif = virementDto.getMotif();

		if (compteEmetteur == null) {
			System.out.println("Compte Emetteur Non existant");
			throw new CompteNonExistantException("Compte Emetteur Non existant");
		}

		if (compteBeneficiaire == null) {
			System.out.println("Compte Beneficiaire Non existant");
			throw new CompteNonExistantException("Compte Beneficiaire Non existant");
		}

		if (montantVirement.equals(null)) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");

		} else if (montantVirement.compareTo(new BigDecimal(0))==0  ) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");

		} else if (montantVirement.compareTo(new BigDecimal(10))<0) {
			System.out.println("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");

		} else if (montantVirement.compareTo(new BigDecimal(MONTANT_MAXIMAL))==1) {
			System.out.println("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");

		}

		if (motif.length() < 0) {
			LOGGER.error("le nombre de virement réalisé dépasse 10");
			throw new TransactionException("Motif vide");

		}

		if (compteEmetteur.getSolde().intValue() - montantVirement.intValue() < 0) {
			LOGGER.error("Solde insuffisant pour l'utilisateur");
		}


		BigDecimal nouveauEmetteurSolde = compteEmetteur.getSolde().subtract(montantVirement);
		compteEmetteur.setSolde(nouveauEmetteurSolde);
		compteRepository.save(compteEmetteur);

		BigDecimal nouveauBeneficiaireSolde = new BigDecimal(
				compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue());
		compteBeneficiaire.setSolde(nouveauBeneficiaireSolde);
		compteRepository.save(compteBeneficiaire);

		Virement virement = new Virement();
		virement.setDateExecution(virementDto.getDate());
		virement.setCompteBeneficiaire(compteBeneficiaire);
		virement.setCompteEmetteur(compteEmetteur);
		virement.setMontantVirement(montantVirement);

		virementRepository.save(virement);

		auditService.auditVirement("Virement depuis " + compteEmetteur.getNrCompte() + " vers "
				+ compteBeneficiaire.getNrCompte() + " d'un montant de " + montantVirement.toString());

	}

	@Override
	public void createVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException {

		Compte compteBeneficiaire = compteRepository.findByRib(versementDto.getRibCompteBeneficiaire());
		String nom_prenom_emetteur = versementDto.getNom_prenom_emetteur();
		BigDecimal montantVirement = versementDto.getMontantVirement();
		String motifVersement = versementDto.getMotifVersement();

		if (compteBeneficiaire == null) {
			System.out.println("Compte Beneficiaire Non existant");
			throw new CompteNonExistantException("Compte Beneficiaire Non existant");
		}

		if (montantVirement==null) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");

		} else if (montantVirement.compareTo(new BigDecimal(0))==0) {
			System.out.println("Montant vide");
			throw new TransactionException("Montant vide");

		} else if (montantVirement.compareTo(new BigDecimal(10))<0 ) {
			System.out.println("Montant minimal de virement non atteint");
			throw new TransactionException("Montant minimal de virement non atteint");

		} else if (montantVirement.intValue() > MONTANT_MAXIMAL) {
			System.out.println("Montant maximal de virement dépassé");
			throw new TransactionException("Montant maximal de virement dépassé");

		}

		if (motifVersement.length() < 0) {
			System.out.println("Motif vide");
			throw new TransactionException("Motif vide");

		}

		BigDecimal nouveauBeneficiaireSolde = new BigDecimal(
				compteBeneficiaire.getSolde().intValue() + montantVirement.intValue());
		compteBeneficiaire.setSolde(nouveauBeneficiaireSolde);
		compteRepository.save(compteBeneficiaire);

		Versement versement = new Versement();
		versement.setCompteBeneficiaire(compteBeneficiaire);
		versement.setDateExecution(versementDto.getDate());
		versement.setMontantVirement(montantVirement);
		versement.setMotifVersement(motifVersement);
		versement.setNom_prenom_emetteur(nom_prenom_emetteur);
		versementRepository.save(versement);

		auditService.auditVersement("Versement depuis " + nom_prenom_emetteur + " vers "
				+ compteBeneficiaire.getNrCompte() + " d'un montant de " + montantVirement.toString());

	}

}
