package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.IBanqueService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

//@RestController(value = "/virements")
@RestController
@RequestMapping(value = "/banque")
class BanqueController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(BanqueController.class);

    IBanqueService banqueService;

    public BanqueController(IBanqueService banqueService) {
    	
		this.banqueService = banqueService;
	}

	@GetMapping("virements")
    List<VirementDto> loadAllVirements() {
        return banqueService.loadAllVirement();
    }
	
	@GetMapping("versements")
	List<VersementDto> loadAllVersements(){
		return banqueService.loadAllVersement();
	}

    @GetMapping("comptes")
    List<Compte> loadAllCompte() {
       return banqueService.loadAllCompte();
    }

    @GetMapping("utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        return banqueService.loadAllUtilisateur();
    }

    @PostMapping("/virement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createVirement(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    	
        banqueService.createVirement(virementDto);
    }

    @PostMapping("/versement")
    @ResponseStatus(HttpStatus.CREATED)
	public void createVersement(@RequestBody VersementDto versementDto)
			throws CompteNonExistantException, TransactionException {
    	banqueService.createVersement(versementDto);
    }
  
}
